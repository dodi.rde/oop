<?php
require "animal.php";
require "ape.php";
require "frog.php";

$sheep = new animal("shaun");

echo "nama: " . $sheep->name; // "shaun"
echo "<br>";
echo "jumlah kaki: " . $sheep->legs; // 2
echo $sheep->cold_blooded; // false
echo "<br>";
echo "<br>";

$sungokong = new ape("kera sakti");
echo "nama: " . $sungokong->name;
echo "<br>";
echo "jumlah kaki: " . $sungokong->legs;
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br>";
echo "<br>";

$kodok = new frog("buduk");
echo "nama: " . $kodok->name;
echo "<br>";
echo "jumlah kaki: " . $kodok->legs;
echo "<br>";
$kodok->jump(); // "hop hop"

?>